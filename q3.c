#include <stdio.h>

int main()
{
	int a,b,c;
	printf("Enter the two numbers :\n");
	scanf("%d%d",&a,&b);
	
	printf("Before swapping First num = %d Second num = %d\n",a,b);
	
	c = a;
	a = b;
	b = c;
	
	printf("After swapping First num = %d Second num = %d\n",a,b);
	return 0;
}
